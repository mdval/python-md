from python_md import Markdown


if __name__ == '__main__':
    md = Markdown('examples.md')
    content = md.h1('H1')
    content += md.h2('H2')
    content += md.h3('H3')
    content += md.h4('H4')
    content += md.h5('H5')
    content += md.h6('H6')

    content += md.hr()

    content += md.italic('italic')
    content += '\n\n'
    content += md.bold('bold')
    content += '\n\n'
    content += md.strikethrough('strikethrough')
    content += '\n\n'
    content += md.italic(md.bold(md.strikethrough('italic bold and strikethrough')))
    content += '\n\n'

    content += md.italic('banana and banana and apple', ['banana', 'apple'])
    content += '\n\n'
    content += md.bold('banana and banana and apple', ['banana', 'apple'])
    content += '\n\n'
    content += md.strikethrough('banana and banana and apple', ['banana', 'apple'])
    content += '\n\n'

    content += md.inline_code('inline code')
    content += '\n\n'
    content += md.inline_code('banana and banana and apple', ['banana', 'apple'])
    content += '\n\n'

    content += md.link('Python Markdown', 'https://gitlab.com/mdval/python-md')
    content += '\n\n'

    header = ['this', 'is', 'a', 'table', 'header']
    body = [
        ['this', 'is', 'the', 'first', 'row'],
        ['this', 'is', 'the', 'second', 'row'],
        ['this', 'is', 'the', 'third', 'row'],
    ]
    content += md.table(header, body)

    _list = ['a', ['aa', ['aaa']], 'b', ['bb', ['bbb']]]
    content += md.unordered_list(_list)
    content += md.ordered_list(_list)

    content += md.code_block('python -m unittest test_markdown.py')
    content += md.code_block('''from python_md import Markdown

md = Markdown('examples.md')
md.write_to_file('Hello!')''', 'python')

    content += md.blockquotes('This is a blockquote!')

    md.write_to_file(content, 'w+')
