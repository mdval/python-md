# Changelog
All notable changes to this project will be documented in this file.

## [1.0.2] - 2021-06-15
### Added
- Keywords in setup file
### Fixed
- Examples

## [1.0.1] - 2021-05-20
### Fixed
- Directory structure

## [1.0.0] - 2021-05-20
- Initial release
