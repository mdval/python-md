tests:
	python -m unittest test_markdown.py

examples:
	python examples.py

build: clean
	 python setup.py sdist bdist_wheel

testpypi:
	python3 -m twine upload --repository testpypi dist/*

pypi:
	python3 -m twine upload dist/*

clean:
	rm -rf build dist *.egg-info

.PHONY: all
