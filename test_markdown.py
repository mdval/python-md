import os
import unittest
from python_md import Markdown, MarkdownException


class TestMarkdownException(unittest.TestCase):
    def setUp(self) -> None:
        self.md = Markdown()

    def test_table_bad_body(self):
        header = ["this", "is", "a", "table", "header"]
        body = ["this", "is", "a", "bad", "body"]
        self.assertRaises(MarkdownException, self.md.table, header, body)

    def test_table_bad_sizes(self):
        header = ["this", "is", "a", "table", "header"]
        body = [
            ["this", "is", "the", "first", "row"],
            ["this", "is", "the", "bad", "second", "row"],
            ["this", "is", "the", "third", "row"],
        ]
        self.assertRaises(MarkdownException, self.md.table, header, body)


class TestMarkdown(unittest.TestCase):
    def setUp(self) -> None:
        self.md = Markdown()

    def test_h1(self):
        self.assertEqual(self.md.h1("header 1"), "# header 1\n\n")

    def test_h2(self):
        self.assertEqual(self.md.h2("header 2"), "## header 2\n\n")

    def test_h3(self):
        self.assertEqual(self.md.h3("header 3"), "### header 3\n\n")

    def test_h4(self):
        self.assertEqual(self.md.h4("header 4"), "#### header 4\n\n")

    def test_h5(self):
        self.assertEqual(self.md.h5("header 5"), "##### header 5\n\n")

    def test_h6(self):
        self.assertEqual(self.md.h6("header 6"), "###### header 6\n\n")

    def test_italic_single(self):
        self.assertEqual(self.md.italic("e ae e ea e", ["e"]), "*e* ae *e* ea *e*")

    def test_italic_multiple(self):
        self.assertEqual(self.md.italic("e ae e ea e", ["ae", "ea"]), "e *ae* e *ea* e")

    def test_italic_all(self):
        self.assertEqual(self.md.italic("e ae e ea e"), "*e ae e ea e*")

    def test_bold_single(self):
        self.assertEqual(self.md.bold("e ae e ea e", ["e"]), "__e__ ae __e__ ea __e__")

    def test_bold_multiple(self):
        self.assertEqual(self.md.bold("e ae e ea e", ["ae", "ea"]), "e __ae__ e __ea__ e")

    def test_bold_all(self):
        self.assertEqual(self.md.bold("e ae e ea e"), "__e ae e ea e__")

    def test_strikethrough_single(self):
        self.assertEqual(self.md.strikethrough("e ae e ea e", ["e"]), "~~e~~ ae ~~e~~ ea ~~e~~")

    def test_strikethrough_multiple(self):
        self.assertEqual(self.md.strikethrough("e ae e ea e", ["ae", "ea"]), "e ~~ae~~ e ~~ea~~ e")

    def test_strikethrough_all(self):
        self.assertEqual(self.md.strikethrough("e ae e ea e"), "~~e ae e ea e~~")

    def test_inline_code_single(self):
        self.assertEqual(self.md.inline_code("e ae e ea e", ["e"]), "`e` ae `e` ea `e`")

    def test_inline_code_multiple(self):
        self.assertEqual(self.md.inline_code("e ae e ea e", ["ae", "ea"]), "e `ae` e `ea` e")

    def test_inline_code_all(self):
        self.assertEqual(self.md.inline_code("e ae e ea e"), "`e ae e ea e`")

    def test_code_block(self):
        self.assertEqual(self.md.code_block("python -m unittest tests/test_markdown.py"),
                         "```\npython -m unittest tests/test_markdown.py\n```\n\n")

    def test_code_block_with_lang(self):
        self.assertEqual(self.md.code_block("python -m unittest tests/test_markdown.py", "shell"),
                         "```shell\npython -m unittest tests/test_markdown.py\n```\n\n")

    def test_link(self):
        self.assertEqual(
            self.md.link("Python Markdown", "https://gitlab.com/mdval/python-md"),
            "[Python Markdown](https://gitlab.com/mdval/python-md)"
        )

    def test_table_header(self):
        self.assertEqual(self.md.table_header(["this", "is", "a", "table", "header"]),
                         "|this|is|a|table|header|\n"
                         "|---|---|---|---|---|\n")

    def test_table_body(self):
        self.assertEqual(self.md.table_row(["this", "is", "a", "table", "row"]), "|this|is|a|table|row|\n")

    def test_table(self):
        header = ["this", "is", "a", "table", "header"]
        body = [
            ["this", "is", "the", "first", "row"],
            ["this", "is", "the", "second", "row"],
            ["this", "is", "the", "third", "row"],
        ]
        self.assertEqual(self.md.table(header, body),
                         "|this|is|a|table|header|\n"
                         "|---|---|---|---|---|\n"
                         "|this|is|the|first|row|\n"
                         "|this|is|the|second|row|\n"
                         "|this|is|the|third|row|\n")

    def test_unordered_list(self):
        unordered_list = ["a", ["aa", ["aaa"]], "b", ["bb", ["bbb"]]]
        self.assertEqual(self.md.unordered_list(unordered_list), "- a\n\t- aa\n\t\t- aaa\n- b\n\t- bb\n\t\t- bbb\n\n")

    def test_ordered_list(self):
        ordered_list = ["a", ["aa", ["aaa"]], "b", ["bb", ["bbb"]]]
        self.assertEqual(self.md.ordered_list(ordered_list), "1. a\n\t1. aa\n\t\t1. aaa\n1. b\n\t1. bb\n\t\t1. bbb\n\n")

    def test_write(self):
        md = Markdown('test.md')
        md.write_to_file("just a simple paragraph")
        with open('test.md') as f:
            content = f.read()
        os.remove('test.md')
        self.assertEqual(content, "just a simple paragraph")

    def test_hr(self):
        self.assertEqual(self.md.hr(), "\n\n---\n")

    def test_blockquotes(self):
        self.assertEqual(self.md.blockquotes("I'm a blockquote!"), "> I'm a blockquote!\n\n")


if __name__ == "__main__":
    unittest.main()
